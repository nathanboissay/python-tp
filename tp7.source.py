""" TP7 une application complète
    ATTENTION VOUS DEVEZ METTRE DES DOCSTRING A TOUTES VOS FONCTIONS
"""
from email import message
import readline


def afficher_menu(titre, liste_options):
    print(f'+{"-"*(len(titre)+2)}+')
    print(f'| {titre} |')
    print(f'+{"-"*(len(titre)+2)}+')
    nb = 1
    for i in range(len(liste_options)):
        print(f"{nb} -> {liste_options[nb-1]}")
        nb +=1
    
    

def demander_nombre(message, borne_max):
    nb = input(f'{message}\n')
    if nb.isdecimal() and int(nb) <= int(borne_max) and int(nb) > 0 :
        return int(nb)
    else : 
        return None



def menu(titre, liste_options):
    afficher_menu(titre,liste_options)
    rep = demander_nombre("Entrez votre choix [1-4]",len(liste_options))

    if rep == None:
        return None
    if int(rep) > 0 and int(rep) <= len(liste_options) :
        return int(rep)
    else :
        return None



def programme_principal():
    liste_options = ["Charger un fichier",
                     "Rechercher la population d'une commune","Liste des communes commencant par",
                     "commune_plus_peuplee_departement","nombre_de_communes_tranche_pop","ajouter_trier","Afficher la population d'un département", 
                     "Quitter"]
    liste_communes = []
    liste_villes = []
    while True:
      
        rep = menu("MENU DE MON APPLICATION", liste_options)
        
        if rep is None:
            print("Cette option n'existe pas")
        elif rep == 1:
            print("Vous avez choisi", liste_options[rep - 1])
            nom_fic = input("choisissez le nom d'un fichier \n")
            liste_villes = charger_fichier_population(nom_fic)
            print("Il y a",len(liste_villes),"fichiers")
        elif rep == 2:
            print("Vous avez choisi", liste_options[rep - 1])
            nom_communes = input("Entree le nom de la communes que vous cherchez \n")
            population_d_une_commune(liste_villes,nom_communes)
        elif rep == 3 :
            print("Vous avez choisi", liste_options[rep - 1])
            print(liste_des_communes_commencant_par(liste_villes,debut_nom=input("Entrez le début d'un nom de commune")))
        elif rep== 4 :
            print("Vous avez choisi", liste_options[rep - 1])
            print(commune_plus_peuplee_departement(liste_villes, num_dpt=input("Entrez un numéro de département")))
        elif rep == 5:
            print("Vous avez choisi", liste_options[rep - 1])
            print(nombre_de_communes_tranche_pop(liste_villes, pop_min=input("Entrez la tranche min"), pop_max=input("Entrez la tranche max")))
        elif rep == 6 :
            print("Vous avez choisi", liste_options[rep - 1])
            print(trier_la_liste(liste_villes))
        elif rep == 7:
            print("Vous avez choisi", liste_options[rep - 1])
        elif rep == 8:
            print("Vous avez choisi", liste_options[rep - 1])
            break
        
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!")
    




       
def charger_fichier_population(nom_fic):
    liste = []
    liste_final = []
    fic=open(nom_fic,'r')
    fic.readline()
    for ligne in fic :
        liste.append(ligne.split(";"))
    for i in range(len(liste)) :
        liste_final.append((liste[i][0],liste[i][1],liste[i][4][0:-1]))
    
    return liste_final

def population_d_une_commune(liste_pop, nom_commune):
    
    for i in range(len(liste_pop)) :
        if liste_pop[i][1] == nom_commune :
            print("Il y a ",liste_pop[i][2], "Habitant dans", liste_pop[i][1])
    

def liste_des_communes_commencant_par(liste_pop, debut_nom):
    liste = []
    for i in range(len(liste_pop)) :
        if debut_nom in liste_pop[i][1] :
            liste.append(liste_pop[i][1])
    return liste

def commune_plus_peuplee_departement(liste_pop, num_dpt):
    max = ""
    ind = 0
    while max == "" :
        if liste_pop[ind][0][0:2] == num_dpt :
            max = liste_pop[ind]
        ind += 1
    
    for i in range(len(liste_pop)) :
        if liste_pop[i][0][0:2] == num_dpt :
            if int(liste_pop[i][2]) > int(max[2]) :
                max = liste_pop[i]
    return max[1]


def nombre_de_communes_tranche_pop(liste_pop, pop_min, pop_max):
    liste_ville = []
    for i in range(len(liste_pop)) :
        if int(pop_min) < int(liste_pop[i][2]) < int(pop_max) :
            liste_ville.append(liste_pop[i][1]) 
    return liste_ville

def trier_la_liste(liste_villes):
    liste_trié = []
    tuple_ajout = []
    ind = 0
    print(liste_villes)
    for i in range(len(liste_villes)) :
        
        while tuple_ajout == [] and ind < (len(liste_villes)+1):
            if liste_villes[ind] not in liste_trié :
                tuple_ajout = liste_villes[ind]
            ind += 1
        for j in range(len(liste_villes)) :

            if liste_villes[j] not in liste_trié and liste_villes[j][2] > tuple_ajout[2] :
                tuple_ajout = liste_villes[j]
        
        liste_trié.append(tuple_ajout)
        tuple_ajout = []
        print(liste_trié)
    return liste_trié
def place_top(commune, liste_pop):
    ...

def ajouter_trier(commune, liste_pop, taille_max):
    ...


def top_n_population(liste_pop, nbre):
    ...


def population_par_departement(liste_pop):
    ...

def sauve_population_dpt(nom_fic, liste_pop_dep):
    ...

# appel au programme principal
programme_principal()
